package com.astromedicomp.gles_checker_board_texture;

public class GLESMacros
{
    // attribute index
    public static final int VDG_ATTRIBUTE_VERTEX=0;
    public static final int VDG_ATTRIBUTE_COLOR=1;
    public static final int VDG_ATTRIBUTE_NORMAL=2;
    public static final int VDG_ATTRIBUTE_TEXTURE0=3;
}
