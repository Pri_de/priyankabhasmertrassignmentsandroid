package com.astromedicomp.winmyviewevent;

import android.widget.TextView;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

public class MyView extends TextView implements OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	MyView(Context context)
	{
		super(context);
		
		setTextColor(Color.rgb(255,128,0));
		setText("Hello World!!");
		setGravity(Gravity.CENTER);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		
		gestureDetector.setOnDoubleTapListener(this);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		
		// code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return(true);
				
	}
	
	
	// abstract mothod from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		setText("Double Tap");
		return(true);
	}
	
	// abstract mothod from OnDoubleTapListener so must be implemented
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	// abstract mothod from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		setText("Single Tap");
		return(true);
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		setText("Long Press");
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		setText("Scroll");
		return(true);
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	// abstract mothod from OnGestureListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

}
























