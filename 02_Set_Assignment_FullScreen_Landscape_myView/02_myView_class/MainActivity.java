package com.astromedicomp.winmyview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.graphics.Color;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.view.Gravity;

public class MainActivity extends Activity {

	private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // super must be always first line
		
		// get rid of the action bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//set window color as black
		this.getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
				
		// set orientaion
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		
		myView = new MyView(this);
		
		
        setContentView(myView);
    }
}
