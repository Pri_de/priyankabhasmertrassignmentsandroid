package com.astromedicomp.winmyview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.Gravity;
import android.content.Context;
import android.graphics.Color;

public class MyView extends TextView{

	MyView(Context context)
	{
		super(context);
		
		setTextColor(Color.rgb(255,128,0));
		setText("Hello World!!");
		setGravity(Gravity.CENTER);
		
	}
	   
}
