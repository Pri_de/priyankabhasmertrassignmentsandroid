package com.astromedicomp.gles_24_spheres_pf_lighting;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import java.lang.Math;
import android.opengl.Matrix; // for Matrix math

// A view for OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    
    private GestureDetector gestureDetector;
	    
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int numElements;
    private int numVertices;
    
    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private float[] light_ambient = {0.0f,0.0f,0.0f,1.0f};
    private float[] light_diffuse = {1.0f,1.0f,1.0f,1.0f};
    private float[] light_specular = {1.0f,1.0f,1.0f,1.0f};
    private float[] light_position = { 100.0f,100.0f,100.0f,1.0f };
    
    private int  modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int  laUniform, ldUniform, lsUniform, lightPositionUniform;
    private int  kaUniform, kdUniform, ksUniform, materialShininessUniform;

    private int doubleTapUniform;

    private float[] perspectiveProjectionMatrix=new float[16]; // 4x4 matrix
    
    private int doubleTap; // for lights
	private int singleTap; // for light rotation axis change
	
	//enum RESULTCODE{
	private int R_SUCCESS = 0;
	private int R_STACKOVERFLOW = -1;
	
	private float PI = 3.142f;
	//};
	
	private float angleLight;

	// Matrix stack
	private int currentStackIndex = -1;
	private float mStack[] = new float[32*16];
	
	private float modelMatrix[]=new float[16];
	
	private float[] materialProperties= //new MaterialProperties[24]
	{
		// ***** 1st sphere on 1st columnf,  emerald *****
		0.0215f, 0.1745f, 0.0215f, 1.0f, //Ambient
		0.0768f, 0.61424f, 0.07568f, 1.0f, //Diffused
		0.633f, 0.727811f, 0.633f, 1.0f, //Specular
		0.6f * 128f, //Shininess

		// ***** 2nd sphere on 1st columnf,  jade *****
		0.135f, 0.2225f, 0.1575f, 1.0f, //Ambient
		0.54f, 0.89f, 0.63f, 1.0f, //Diffused
		0.316228f, 0.316228f, 0.316228f, 1.0f, //Specular
		0.1f * 128f, //Shininess

		// ***** 3rd sphere on 1st column obsidian *****
		0.05375f, 0.05f, 0.06625f, 1.0f, //Ambient
		0.18275f, 0.17f, 0.22525f, 1.0f, //Diffused
		0.332741f, 0.328634f, 0.346435f, 1.0f, //Specular
		0.3f * 128f, //Shininess

		// ***** 4th sphere on 1st column pearl *****
		0.25f, 0.20725f, 0.20725f, 1.0f, //Ambient
		1.0f, 0.829f, 0.829f, 1.0f, //Diffused
		0.296648f, 0.296648f, 0.296648f, 1.0f, //Specular
		0.088f * 128f, //Shininess

		// ***** 5th sphere on 1st column ruby *****
		0.1745f, 0.01175f, 0.01175f, 1.0f, //Ambient
		0.61424f, 0.04136f, 0.04136f, 1.0f, //Diffused
		0.727811f, 0.626959f, 0.626959f, 1.0f, //Specular
		0.6f * 128f, //Shininess

		// ***** 6th sphere on 1st column turquoise *****
		0.1f, 0.18725f, 0.1745f, 1.0f, //Ambient
		0.396f, 0.74151f, 0.69102f, 1.0f, //Diffused
		0.297254f, 0.30829f, 0.306678f, 1.0f, //Specular
		0.1f * 128f, //Shininess

		// ***** 1st sphere on 2nd column brass *****
		0.329412f, 0.223529f, 0.027451f, 1.0f, //Ambient
		0.780392f, 0.568627f, 0.113725f, 1.0f, //Diffused
		0.992157f, 0.941176f, 0.807843f, 1.0f, //Specular
		0.21794872f * 128f, //Shininess

		// ***** 2nd sphere on 2nd column bronze *****
		0.2125f, 0.1275f, 0.054f, 1.0f, //Ambient
		0.714f, 0.4284f, 0.18144f, 1.0f, //Diffused
		0.393548f, 0.271906f, 0.166721f, 1.0f, //Specular
		0.2f * 128f, //Shininess

		// ***** 3rd sphere on 2nd column chrome *****
		0.25f, 0.25f, 0.25f, 1.0f, //Ambient
		0.4f, 0.4f, 0.4f, 1.0f, //Diffused
		0.774597f, 0.774597f, 0.774597f, 1.0f, //Specular
		0.6f * 128f, //Shininess

		// ***** 4th sphere on 2nd column copper *****
		0.19125f, 0.0735f, 0.0225f, 1.0f, //Ambient
		0.7038f, 0.27048f, 0.0828f, 1.0f, //Diffused
		0.256777f, 0.137622f, 0.086014f, 1.0f, //Specular
		0.1f * 128f, //Shininess

		// ***** 5th sphere on 2nd column gold *****
		0.24725f, 0.1995f, 0.0745f, 1.0f, //Ambient
		0.75164f, 0.60648f, 0.22648f, 1.0f, //Diffused
		0.628281f, 0.555802f, 0.366065f, 1.0f, //Specular
		0.4f * 128f, //Shininess

		// ***** 6th sphere on 2nd column silver *****
		0.19225f, 0.19225f, 0.19225f, 1.0f, //Ambient
		0.50754f, 0.50754f, 0.50754f, 1.0f, //Diffused
		0.508273f, 0.508273f, 0.508273f, 1.0f, //Specular
		0.4f * 128f, //Shininess

		// ***** 1st sphere on 3rd column black *****
		0.0f, 0.0f, 0.0f, 1.0f, //Ambient
		0.0f, 0.0f, 0.0f, 1.0f, //Diffused
		0.50f, 0.50f, 0.50f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 2nd sphere on 3rd column cyan *****
		0.0f, 0.1f, 0.06f, 1.0f, //Ambient
		0.0f, 0.50980392f, 0.50980392f, 1.0f, //Diffused
		0.50196078f, 0.50196078f, 0.50196078f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 3rd sphere on 2nd column green *****
		0.0f, 0.0f, 0.0f, 1.0f, //Ambient
		0.1f, 0.35f, 0.1f, 1.0f, //Diffused
		0.45f, 0.55f, 0.45f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 4th sphere on 3rd column red *****
		0.0f, 0.0f, 0.0f, 1.0f, //Ambient
		0.5f, 0.0f, 0.0f, 1.0f, //Diffused
		0.7f, 0.6f, 0.6f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 5th sphere on 3rd column white *****
		0.0f, 0.0f, 0.0f, 1.0f, //Ambient
		0.55f, 0.55f, 0.55f, 1.0f, //Diffused
		0.70f,  0.70f,  0.70f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 6th sphere on 3rd column yellow plastic *****
		0.0f,  0.0f,  0.0f, 1.0f, //Ambient
		0.5f,  0.5f,  0.0f, 1.0f, //Diffused
		0.60f,  0.60f,  0.50f, 1.0f, //Specular
		0.25f * 128f, //Shininess

		// ***** 1st sphere on 4th column black *****
		0.02f,  0.02f,  0.02f, 1.0f, //Ambient
		0.01f,  0.01f,  0.01f, 1.0f, //Diffused
		0.4f,  0.4f,  0.4f, 1.0f, //Specular
		0.078125f * 128f, //Shininess

		// ***** 2nd sphere on 4th column cyan *****
		0.0f,  0.05f,  0.05f, 1.0f, //Ambient
		0.4f,  0.5f,  0.5f, 1.0f, //Diffused
		0.04f,  0.7f,  0.7f, 1.0f, //Specular
		0.078125f * 128f, //Shininess

		// ***** 3rd sphere on 4th column green *****
		0.0f,  0.05f,  0.0f,  1.0f, //Ambient
		0.4f,  0.5f,  0.4f,  1.0f, //Diffused
		0.04f,  0.7f,  0.04f,  1.0f, //Specular
		0.078125f * 128f, //Shininess

		// ***** 4th sphere on 4th column red *****
		0.05f,  0.0f,  0.0f,  1.0f, //Ambient
		0.5f,  0.4f,  0.4f,  1.0f, //Diffused
		0.7f,  0.04f,  0.04f,  1.0f, //Specular
		0.078125f * 128f, //Shininess

		// ***** 5th sphere on 4th column white *****
		0.05f,  0.05f,  0.05f,  1.0f, //Ambient
		0.5f,  0.5f,  0.5f,  1.0f, //Diffused
		0.7f,  0.7f,  0.7f,  1.0f, //Specular
		0.078125f * 128f, //Shininess

		// ***** 6th sphere on 4th column yellow rubber *****
		0.05f,  0.05f,  0.0f,  1.0f, //Ambient
		0.5f,  0.5f,  0.4f,  1.0f, //Diffused
		0.7f,  0.7f,  0.04f,  1.0f, //Specular
		0.078125f * 128f, //Shininess

	};
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        
        context=drawingContext;

        // accordingly set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set Renderer for drawing on the GLSurfaceView
        setRenderer(this);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }
    
    // overriden method of GLSurfaceView.Renderer ( Init Code )
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // get OpenGL-ES version
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VDG: OpenGL-ES Version = "+glesVersion);
        // get GLSL version
        String glslVersion=gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VDG: GLSL Version = "+glslVersion);

        initialize(gl);
    }
 
    // overriden method of GLSurfaceView.Renderer ( Chnge Size Code )
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // code
        int eventaction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // code
        doubleTap++;
        if(doubleTap > 1)
            doubleTap=0;
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        // code
		        // code
        singleTap++;
        if(singleTap > 2)
            singleTap=0;
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

	// matrix stack
	int pushMatrix() {

		if (currentStackIndex > 31)
		{
			return R_STACKOVERFLOW;
		}
		else
		{
			currentStackIndex++;
			System.arraycopy(modelMatrix,0,mStack,currentStackIndex*16,16);
			//std::memcpy(mStack[currentStackIndex], modelMatrix, sizeof(float)*16));
			return R_SUCCESS;
		}
	}

	int popMatrix() {

		if (currentStackIndex > 31 && currentStackIndex < 0)
		{
			return R_STACKOVERFLOW;
		}
		else
		{
			System.arraycopy(mStack,currentStackIndex*16,modelMatrix,0,16);
			//memcpy(modelMatrix, mStack[currentStackIndex], sizeof(float)*16));
			currentStackIndex--;
			return R_SUCCESS;
		}
	}


    private void initialize(GL10 gl)
    {
        // ***********************************************
        // Vertex Shader
        // ***********************************************
        // create shader
        vertexShaderObject=GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);
        
        // vertex shader source code
        final String vertexShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform mediump int u_double_tap;"+
         "uniform vec4 u_light_position;"+
         "out vec3 transformed_normals;"+
         "out vec3 light_direction;"+
         "out vec3 viewer_vector;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;"+
         "light_direction = vec3(u_light_position) - eye_coordinates.xyz;"+
         "viewer_vector = -eye_coordinates.xyz;"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
        );
 
        // provide source code to shader
        GLES31.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        
        // compile shader & check for errors
        GLES31.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;
        GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
           }
        }

        // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 transformed_normals;"+
         "in vec3 light_direction;"+
         "in vec3 viewer_vector;"+
         "out vec4 FragColor;"+
         "uniform vec3 u_La;"+
         "uniform vec3 u_Ld;"+
         "uniform vec3 u_Ls;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "uniform int u_double_tap;"+
         "void main(void)"+
         "{"+
         "vec3 phong_ads_color;"+
         "if(u_double_tap==1)"+
         "{"+
         "vec3 normalized_transformed_normals=normalize(transformed_normals);"+
         "vec3 normalized_light_direction=normalize(light_direction);"+
         "vec3 normalized_viewer_vector=normalize(viewer_vector);"+
         "vec3 ambient = u_La * u_Ka;"+
         "float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);"+
         "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"+
         "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"
        );

        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // create shader program
        shaderProgramObject=GLES31.glCreateProgram();
        
        // attach vertex shader to shader program
        GLES31.glAttachShader(shaderProgramObject,vertexShaderObject);
        
        // attach fragment shader to shader program
        GLES31.glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader attributes
        GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
        GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

        // link the two shaders together to shader program object
        GLES31.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if (iShaderProgramLinkStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // get uniform locations
        modelMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        doubleTapUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_double_tap");
        
        laUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_La");
        ldUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld");
        lsUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls");
        lightPositionUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_light_position");

        kaUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ka");
        kdUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ks");
        materialShininessUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_material_shininess");

        // *** vertices, colors, shader attribs, vbo, vao initializations ***
        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES31.glGenVertexArrays(1,vao_sphere,0);
        GLES31.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES31.glGenBuffers(1,vbo_sphere_position,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES31.glGenBuffers(1,vbo_sphere_normal,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES31.glGenBuffers(1,vbo_sphere_element,0);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES31.glBindVertexArray(0);

        // enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        // depth test to do
        GLES31.glDepthFunc(GLES31.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES31.glEnable(GLES31.GL_CULL_FACE);
        
        // Set the background color
        GLES31.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
        
        // initialization
        doubleTap=0;
		singleTap =0;
        
        // set projectionMatrix to identitu matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		
	}
    
    private void resize(int width, int height)
    {
        // code
        GLES31.glViewport(0, 0, width, height);
        
        // calculate the projection matrix
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f); // typecasting is IMP
    }
    
    public void display()
    {
		float xPos = -5.0f;
		float yPos = 3.0f;
		int sphereCounter = 0;
        // code
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);
        
        // use shader program
        GLES31.glUseProgram(shaderProgramObject);

        if(doubleTap==1)
        {
			
			if (singleTap == 0)
			{
				light_position[0] = 0.0f;
				light_position[1] = 40 * (float)Math.sin(angleLight);
				light_position[2] = 40 * (float)Math.cos(angleLight);
			}
			else if (singleTap == 1)
			{
				light_position[1] = 0.0f;
				light_position[0] = 40 * (float)Math.sin(angleLight);
				light_position[2] = 40 * (float)Math.cos(angleLight);
			}
			else if (singleTap == 2)
			{
				light_position[2] = 0.0f;
				light_position[0] = 40 * (float)Math.sin(angleLight);
				light_position[1] = 40 * (float)Math.cos(angleLight);
			}
			
            GLES31.glUniform1i(doubleTapUniform, 1);
            
            // setting light's properties
            GLES31.glUniform3fv(laUniform, 1, light_ambient, 0);
            GLES31.glUniform3fv(ldUniform, 1, light_diffuse, 0);
            GLES31.glUniform3fv(lsUniform, 1, light_specular, 0);
            GLES31.glUniform4fv(lightPositionUniform, 1, light_position, 0);
            
            // setting material's properties
           /* GLES31.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES31.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES31.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES31.glUniform1f(materialShininessUniform, material_shininess);*/
        }
        else
        {
            GLES31.glUniform1i(doubleTapUniform, 0);
        }
        
        // OpenGL-ES drawing
        
        float viewMatrix[]=new float[16];
        
        // set modelMatrix and viewMatrix matrices to identity matrix
        Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

		
		Matrix.setLookAtM(viewMatrix, 0, 0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
		
        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.0f,0.0f,-1.5f);
        GLES31.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        // GLES31.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        
        //GLES31.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
        sphereCounter = 0;
		for (int column = 0; column < 6; column++) {

			for (int row = 0; row < 4; row++) {

				pushMatrix();
				Matrix.translateM(modelMatrix, 0, xPos, yPos, -8.0f);
				GLES31.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
				GLES31.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
				
				GLES31.glUniform3fv(kaUniform, 1, materialProperties,sphereCounter*13);
				GLES31.glUniform3fv(kdUniform, 1, materialProperties,sphereCounter*13+4);
				GLES31.glUniform3fv(ksUniform, 1, materialProperties,sphereCounter*13+8);
				GLES31.glUniform1fv(materialShininessUniform, 1, materialProperties,sphereCounter*13+12);
				//setMaterialProperties(gstMaterialProperties[sphereCounter++]);
				// *** bind vao ***
				GLES31.glBindVertexArray(vao_sphere[0]);

				// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
				GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES31.glDrawElements(GLES31.GL_TRIANGLES, numElements, GLES31.GL_UNSIGNED_SHORT, 0);

				// *** unbind vao ***
				GLES31.glBindVertexArray(0);
				popMatrix();
				sphereCounter++;
				yPos -= 2.0f;
			}
			xPos += 2.0f;
			yPos = 3.0f;

		}
		
		
		
		
		
		
		
        /*// bind vao
        GLES31.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES31.glDrawElements(GLES31.GL_TRIANGLES, numElements, GLES31.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES31.glBindVertexArray(0);*/
        
        // un-use shader program
        GLES31.glUseProgram(0);
        
        // render/flush
        requestRender();
		
		update();
    }
	
	void update()
	{
		angleLight += 0.05f;
		if(angleLight >= 2*PI)
			angleLight = 0.0f;
	}
	
    
    void uninitialize()
    {
        // code
        // destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES31.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
