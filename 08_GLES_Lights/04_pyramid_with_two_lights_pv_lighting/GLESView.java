package com.astromedicomp.gles_per_vertex_lighting_pyramid;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix; // for Matrix math

// A view for OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    
    private GestureDetector gestureDetector;
    
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    //private int numElements;
    //private int numVertices;
    
    private int[] vao_pyramid = new int[1];
    private int[] vbo_pyramid_position = new int[1];
    private int[] vbo_pyramid_normal = new int[1];
   // private int[] vbo_sphere_element = new int[1];

	private float lightAmbient_red[] = { 0.0f,0.0f,0.0f,0.0f };
	private float lightDiffuse_red[] = { 1.0f,0.0f,0.0f,0.0f };
	private float lightSpecular_red[] = { 1.0f,0.0f,0.0f,0.0f };
	private float lightPosition_red[] = { 2.0f, 1.0f, 0.0f, 0.0f };

	private float lightAmbient_blue[] = { 0.0f,0.0f,0.0f,1.0f };
	private float lightDiffuse_blue[] = { 0.0f,0.0f,1.0f,0.0f };
	private float lightSpecular_blue[] = { 0.0f,0.0f,1.0f,0.0f };
	private float lightPosition_blue[] = { -2.0f, 1.0f,0.0f,1.0f };
	
    
    private float material_ambient[] = {0.0f,0.0f,0.0f,1.0f};
    private float material_diffuse[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_specular[] = {1.0f,1.0f,1.0f,1.0f};
    private float material_shininess = 50.0f;
    
    private int  modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int  laUniform_red, ldUniform_red, lsUniform_red, lightPositionUniform_red;
	private int  laUniform_blue, ldUniform_blue, lsUniform_blue, lightPositionUniform_blue;
    private int  kaUniform, kdUniform, ksUniform, materialShininessUniform;

    private int doubleTapUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; // 4x4 matrix
    
    private int doubleTap; // for lights
	
	private float angleRotate;
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        
        context=drawingContext;

        // accordingly set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set Renderer for drawing on the GLSurfaceView
        setRenderer(this);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }
    
    // overriden method of GLSurfaceView.Renderer ( Init Code )
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // get OpenGL-ES version
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VDG: OpenGL-ES Version = "+glesVersion);
        // get GLSL version
        String glslVersion=gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VDG: GLSL Version = "+glslVersion);

        initialize(gl);
    }
 
    // overriden method of GLSurfaceView.Renderer ( Chnge Size Code )
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // code
        int eventaction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // code
        doubleTap++;
        if(doubleTap > 1)
            doubleTap=0;
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        // code
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    private void initialize(GL10 gl)
    {
        // ***********************************************
        // Vertex Shader
        // ***********************************************
        // create shader
        vertexShaderObject=GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);
        System.out.println("VDG: Entered initialization");
        // vertex shader source code
        final String vertexShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "in vec4 vPosition;"+
         "in vec3 vNormal;"+
         "uniform mat4 u_model_matrix;"+
         "uniform mat4 u_view_matrix;"+
         "uniform mat4 u_projection_matrix;"+
         "uniform int u_double_tap;"+
         "uniform vec3 u_La_red;"+
         "uniform vec3 u_Ld_red;"+
         "uniform vec3 u_Ls_red;"+
         "uniform vec4 u_light_position_red;"+
         "uniform vec3 u_La_blue;"+
         "uniform vec3 u_Ld_blue;"+
         "uniform vec3 u_Ls_blue;"+
         "uniform vec4 u_light_position_blue;"+
         "uniform vec3 u_Ka;"+
         "uniform vec3 u_Kd;"+
         "uniform vec3 u_Ks;"+
         "uniform float u_material_shininess;"+
         "out vec3 phong_ads_color;"+
         "void main(void)"+
         "{"+
         "if (u_double_tap == 1)"+
         "{"+
         "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;"+
         "vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"+
		 
         "vec3 light_direction = normalize(vec3(u_light_position_red) - eye_coordinates.xyz);"+
         "float tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);"+
         "vec3 ambient = u_La_red * u_Ka;"+
         "vec3 diffuse = u_Ld_red * u_Kd * tn_dot_ld;"+
         "vec3 reflection_vector = reflect(-light_direction, transformed_normals);"+
         "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"+
         "vec3 specular = u_Ls_red * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color=ambient + diffuse + specular;"+
		 
		 "light_direction = vec3(0.0, 0.0, 0.0);" +
		 "light_direction = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz);"+
		 "tn_dot_ld = max(dot(transformed_normals, light_direction),0.0);" +
		 "ambient = u_La_blue * u_Ka;"+
         "diffuse = u_Ld_blue * u_Kd * tn_dot_ld;"+
         "reflection_vector = reflect(-light_direction, transformed_normals);"+
         "specular = u_Ls_blue * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
         "phong_ads_color+=ambient + diffuse + specular;"+
	 
         "}"+
         "else"+
         "{"+
         "phong_ads_color = vec3(1.0, 1.0, 1.0);"+
         "}"+
         "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
         "}"
        );
        
        // provide source code to shader
        GLES31.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
        
        // compile shader & check for errors
        GLES31.glCompileShader(vertexShaderObject);
        int[] iShaderCompiledStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;
        GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
           }
        }

        // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "precision highp float;"+
         "in vec3 phong_ads_color;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = vec4(phong_ads_color, 1.0);"+
         "}"
        );
        
        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompiledStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if (iShaderCompiledStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // create shader program
        shaderProgramObject=GLES31.glCreateProgram();
        
        // attach vertex shader to shader program
        GLES31.glAttachShader(shaderProgramObject,vertexShaderObject);
        
        // attach fragment shader to shader program
        GLES31.glAttachShader(shaderProgramObject,fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader attributes
        GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
        GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");

        // link the two shaders together to shader program object
        GLES31.glLinkProgram(shaderProgramObject);
        int[] iShaderProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if (iShaderProgramLinkStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // get uniform locations
        modelMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        doubleTapUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_double_tap");
        
		//get uniform location from gpu for red light
        laUniform_red = GLES31.glGetUniformLocation(shaderProgramObject, "u_La_red");
        ldUniform_red = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld_red");
        lsUniform_red = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls_red");
        lightPositionUniform_red = GLES31.glGetUniformLocation(shaderProgramObject, "u_light_position_red");

		//get uniform location from gpu for blue light
		laUniform_blue = GLES31.glGetUniformLocation(shaderProgramObject, "u_La_blue");
        ldUniform_blue = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ld_blue");
        lsUniform_blue = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ls_blue");
        lightPositionUniform_blue = GLES31.glGetUniformLocation(shaderProgramObject, "u_light_position_blue");
		
        kaUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ka");
        kdUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Kd");
        ksUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_Ks");
        materialShininessUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_material_shininess");;

        /*// *** vertices, colors, shader attribs, vbo, vao initializations ***
        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();*/
		  // *** vertices, colors, shader attribs, vbo, vao initializations ***
		final float pyramidVertices[] =
		{
			0.0f, 1.0f,  0.0f,
			-1.0f,-1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			0.0f, 1.0f,  0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			0.0f, 1.0f,  0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f,-1.0f, -1.0f,
			0.0f, 1.0f,  0.0f,
			-1.0f,-1.0f, -1.0f,
			-1.0f,-1.0f, 1.0f
		};
		
		// *** vertices, colors, shader attribs, vbo, vao initializations ***
		final float pyramidNormals[] =
		{
			0.0f, 0.0f, 1.0f, // front
			0.0f, 0.0f, 1.0f, // front
			0.0f, 0.0f, 1.0f, // front

			1.0f, 0.0f, 0.0f, // right
			1.0f, 0.0f, 0.0f, // right
			1.0f, 0.0f, 0.0f, // right

			0.0f, 0.0f, -1.0f, // back
			0.0f, 0.0f, -1.0f, // back
			0.0f, 0.0f, -1.0f, // back

			-1.0f, 0.0f, 0.0f,  // left
			- 1.0f, 0.0f, 0.0f,  // left
			- 1.0f, 0.0f, 0.0f  // left
		};
		
		
        // vao
        GLES31.glGenVertexArrays(1,vao_pyramid,0);
        GLES31.glBindVertexArray(vao_pyramid[0]);
        
        // position vbo
        GLES31.glGenBuffers(1,vbo_pyramid_position,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(pyramidVertices);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            pyramidVertices.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES31.glGenBuffers(1,vbo_pyramid_normal,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo_pyramid_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(pyramidNormals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(pyramidNormals);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            pyramidNormals.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        
       /* // element vbo
        GLES31.glGenBuffers(1,vbo_sphere_element,0);
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER,0);*/

        GLES31.glBindVertexArray(0);

        // enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        // depth test to do
        GLES31.glDepthFunc(GLES31.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES31.glEnable(GLES31.GL_CULL_FACE);
        
        // Set the background color
        GLES31.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
        
        // initialization
        doubleTap=0;
        
        // set projectionMatrix to identitu matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		System.out.println("VDG: Exit initialization");
    }
    
    private void resize(int width, int height)
    {
		System.out.println("VDG: Entered resize");
        // code
        GLES31.glViewport(0, 0, width, height);
        
        // calculate the projection matrix
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f,100.0f); // typecasting is IMP
		System.out.println("VDG: exit resize");
    }
    
    public void display()
    {
		System.out.println("VDG: Entered display");
        // code
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);
        
        // use shader program
        GLES31.glUseProgram(shaderProgramObject);

        if(doubleTap==1)
        {
            GLES31.glUniform1i(doubleTapUniform, 1);
            
            // setting light's properties red
            GLES31.glUniform3fv(laUniform_red, 1, lightAmbient_red, 0);
            GLES31.glUniform3fv(ldUniform_red, 1, lightDiffuse_red, 0);
            GLES31.glUniform3fv(lsUniform_red, 1, lightSpecular_red, 0);
            GLES31.glUniform4fv(lightPositionUniform_red, 1, lightPosition_red, 0);
            
			// setting light's properties blue
            GLES31.glUniform3fv(laUniform_blue, 1, lightAmbient_blue, 0);
            GLES31.glUniform3fv(ldUniform_blue, 1, lightDiffuse_blue, 0);
            GLES31.glUniform3fv(lsUniform_blue, 1, lightSpecular_blue, 0);
            GLES31.glUniform4fv(lightPositionUniform_blue, 1, lightPosition_blue, 0);
 
            // setting material's properties
            GLES31.glUniform3fv(kaUniform, 1, material_ambient, 0);
            GLES31.glUniform3fv(kdUniform, 1, material_diffuse, 0);
            GLES31.glUniform3fv(ksUniform, 1, material_specular, 0);
            GLES31.glUniform1f(materialShininessUniform, material_shininess);
        }
        else
        {
            GLES31.glUniform1i(doubleTapUniform, 0);
        }
        
        // OpenGL-ES drawing
        float modelMatrix[]=new float[16];
        float viewMatrix[]=new float[16];
        
        // set modelMatrix and viewMatrix matrices to identity matrix
        Matrix.setIdentityM(modelMatrix,0);
        Matrix.setIdentityM(viewMatrix,0);

        // apply z axis translation to go deep into the screen by -1.5,
        // so that pyramid with same fullscreen co-ordinates, but due to above translation will look small
        Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);
		
		Matrix.rotateM(modelMatrix, 0, angleRotate, 0.0f, 1.0f, 0.0f); 
        
        GLES31.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);
        GLES31.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
        GLES31.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);
        
        // bind vao
        GLES31.glBindVertexArray(vao_pyramid[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        //GLES31.glBindBuffer(GLES31.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        //GLES31.glDrawElements(GLES31.GL_TRIANGLES, numElements, GLES31.GL_UNSIGNED_SHORT, 0);
        GLES31.glDrawArrays(GLES31.GL_TRIANGLES, 0, 12);
		
        // unbind vao
        GLES31.glBindVertexArray(0);
        
        // un-use shader program
        GLES31.glUseProgram(0);
        
        // render/flush
        requestRender();
		
		update();
		System.out.println("VDG: exit display");
    }
    
	void update()
	{
		angleRotate+= 0.5f;
		if(angleRotate > 360.0f)
		{
			angleRotate = 0.0f;
		}
		
	}
	
    void uninitialize()
    {
        // code
        // destroy vao
        if(vao_pyramid[0] != 0)
        {
            GLES31.glDeleteVertexArrays(1, vao_pyramid, 0);
            vao_pyramid[0]=0;
        }
        
        // destroy position vbo
        if(vbo_pyramid_position[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_pyramid_position, 0);
            vbo_pyramid_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_pyramid_normal[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_pyramid_normal, 0);
            vbo_pyramid_normal[0]=0;
        }
        
        /*// destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
*/
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}
