package com.astromedicomp.win_gles_blackwhite_trisqr;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

// for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// for matrix math
import android.opengl.Matrix; 


// A view for OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    
    private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_triangle = new int[1];
	private int[] vao_square = new int[1];
	private int[] vbo = new int[1];
	private int mvpUniform;
	
	//4x4 matrix
	private float perspectiveProjectionMatrix[] = new float[16];
    
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        
        context=drawingContext;

        // accordingly set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set Renderer for drawing on the GLSurfaceView
        setRenderer(this);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler' i.e. who is going to handle
    }
    
    // overriden method of GLSurfaceView.Renderer ( Init Code )
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // get OpenGL-ES version
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("PBB: OpenGL-ES Version = "+glesVersion);
		// get glsl versiosn
		String glslVersion = gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("PBB: GLSL Version = "+glslVersion);
        initialize(gl);
    }
 
    // overriden method of GLSurfaceView.Renderer ( Chnge Size Code )
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // code
        int eventaction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        //System.out.println("VDG: "+"Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        //System.out.println("VDG: "+"Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        //System.out.println("VDG: "+"Scroll");
		uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
    
    private void initialize(GL10 gl)
    {
		//******************Vertex Shader************'
		//create shader
		vertexShaderObject = GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode= String.format
		(
		 "#version 310 es"+
		 "\n"+
		 "in vec4 vPosition;"+
		 "uniform mat4 u_mvp_matrix;"+
		 "void main(void)"+
		 "{"+
		 "gl_Position = u_mvp_matrix * vPosition;"+
		 "}"
		 );
		 
		 //provide source code to shader
		 GLES31.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		 
		 // compile shader and check errors
		 GLES31.glCompileShader(vertexShaderObject);
		 int[] iShaderCompileStatus = new int[1];
		 int[] iInfoLogLength = new int[1];
		 String szInfoLog = null;
		 
		 GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		 
		 if(iShaderCompileStatus[0] == GLES31.GL_FALSE)
		 {
			 GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			 if(iInfoLogLength[0]>0)
			 {
				 szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
				 System.out.println("PBB: Vertex Shader Compilation log = "+szInfoLog);
				 uninitialize();
				 System.exit(0);
			 }
		 }
		 
		 // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "precision highp float;"+
         "out vec4 FragColor;"+
         "void main(void)"+
         "{"+
         "FragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
         "}"
        );
        
        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompileStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
		 
		 //create shader program
		 shaderProgramObject = GLES31.glCreateProgram();
		 
		 //attach shaders
		 GLES31.glAttachShader(shaderProgramObject, vertexShaderObject);
		 GLES31.glAttachShader(shaderProgramObject, fragmentShaderObject);
		 
		 GLES31.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		 
		 //link the two shaders together to shader program
		 GLES31.glLinkProgram(shaderProgramObject);
		 int[] iShaderProgramLinkStatus =new int[1];
		 iInfoLogLength[0] = 0; // re-initialize
		 szInfoLog = null;
		 
		 GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		 if(iShaderProgramLinkStatus[0] == GLES31.GL_FALSE)
		 {
			 GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			 if(iInfoLogLength[0] > 0)
			 {
				 szInfoLog = GLES31.glGetShaderInfoLog(shaderProgramObject);
				 System.out.println("VDG: Shader Program Link Log= "+szInfoLog);
				 uninitialize();
				 System.exit(0);
			 }
		 }
		 
		 // get MVP uniform location
		 mvpUniform = GLES31.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		 
		  // *** vertices, colors, shader attribs, vbo, vao initializations ***
        final float triangleVertices[]= new float[]
        {
            0.0f, 1.0f,0.0f, // appex
            -1.0f,-1.0f,0.0f, // left-bottom
            1.0f,-1.0f,0.0f // right-bottom
        };
		
		final float squareVertices[]= new float[]
		{
			1.0f, 1.0f,  0.0f, // right top
			-1.0f, 1.0f, 0.0f, // left top
			-1.0f,  -1.0f, 0.0f, // left bottom
			1.0f,  -1.0f,  0.0f // right bottom
		};

        GLES31.glGenVertexArrays(1,vao_triangle,0);
        GLES31.glBindVertexArray(vao_triangle[0]);

        GLES31.glGenBuffers(1,vbo,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(triangleVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(triangleVertices);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            triangleVertices.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        GLES31.glBindVertexArray(0);
		
		// for squareVertices
		GLES31.glGenVertexArrays(1,vao_square,0);
        GLES31.glBindVertexArray(vao_square[0]);

        GLES31.glGenBuffers(1,vbo,0);
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,vbo[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(squareVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(squareVertices);
        verticesBuffer.position(0);
        
        GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,
                            squareVertices.length * 4,
                            verticesBuffer,
                            GLES31.GL_STATIC_DRAW);
        
        GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES31.GL_FLOAT,
                                     false,0,0);
        
        GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
        GLES31.glBindVertexArray(0);
		
		

        // enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        // depth test to do
        GLES31.glDepthFunc(GLES31.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES31.glEnable(GLES31.GL_CULL_FACE);
	
        // Set the background frame color
        GLES31.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		// set projectionMatrix to identitu matrix
        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }
    
    private void resize(int width, int height)
    {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES31.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height, 1.0f, 100.0f);
    }
	
    public void display()
    {
        // Draw background color
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);
		
		// use shader program
        GLES31.glUseProgram(shaderProgramObject);

        // OpenGL-ES drawing
        float modelViewMatrix[]=new float[16];
        float modelViewProjectionMatrix[]=new float[16];
        
        // set modelview & modelviewprojection matrices to identity
        Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(modelViewMatrix,0,-1.5f,0.0f,-6.0f);
		
		// multiply the modelview and projection matrix to get modelviewprojection matrix
        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
        
        // pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        // bind vao
        GLES31.glBindVertexArray(vao_triangle[0]);
        
        // draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES31.glDrawArrays(GLES31.GL_TRIANGLES,0,3); // 3 (each with its x,y,z ) vertices in triangleVertices array
        
        // unbind vao
        GLES31.glBindVertexArray(0);
        
		// draw square
		// set modelview & modelviewprojection matrices to identity
        Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(modelViewMatrix,0,1.5f,0.0f,-6.0f);
		
		// multiply the modelview and projection matrix to get modelviewprojection matrix
        Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
        
        // pass above modelviewprojection matrix to the vertex shader in 'u_mvp_matrix' shader variable
        // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
        GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
        
        // bind vao
        GLES31.glBindVertexArray(vao_square[0]);
        
        // draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,0,4); // 3 (each with its x,y,z ) vertices in triangleVertices array
        
        // unbind vao
        GLES31.glBindVertexArray(0);
		
		
		
        // un-use shader program
        GLES31.glUseProgram(0);

        // render/flush
        requestRender();
    }
	
	
	void uninitialize()
    {
        // code
        // destroy vao
        if(vao_triangle[0] != 0)
        {
            GLES31.glDeleteVertexArrays(1, vao_triangle, 0);
            vao_triangle[0]=0;
        }
        
		// destroy vao
        if(vao_square[0] != 0)
        {
            GLES31.glDeleteVertexArrays(1, vao_square, 0);
            vao_square[0]=0;
        }
		
        // destroy vao
        if(vbo[0] != 0)
        {
            GLES31.glDeleteBuffers(1, vbo, 0);
            vbo[0]=0;
        }

        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

}
