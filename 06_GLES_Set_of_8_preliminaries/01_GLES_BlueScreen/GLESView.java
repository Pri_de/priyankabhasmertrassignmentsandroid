package com.astromedicomp.win_gles_bluescreen;

import android.content.Context; // for drawing context related
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31; // for OpenGLES 3.2
import android.view.MotionEvent; // for "MotionEvent"
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener

import android.opengl.Matrix;

// A view for OpenGLES3 graphics which also receives touch events
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private final Context context;
    
    private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	
	//4x4 matrix
	private float orthographicProjectionMatrix[] = new float[16];
  
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        
        context=drawingContext;

        // accordingly set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set Renderer for drawing on the GLSurfaceView
        setRenderer(this);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        gestureDetector = new GestureDetector(context, this, null, false); // this means 'handler' i.e. who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler' i.e. who is going to handle
    }
    
    // overriden method of GLSurfaceView.Renderer ( Init Code )
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // get OpenGL-ES version
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("PBB: OpenGL-ES Version = "+glesVersion);
		// get glsl versiosn
		String glslVersion = gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("PBB: GLSL Version = "+glslVersion);
        initialize(gl);
    }
 
    // overriden method of GLSurfaceView.Renderer ( Chnge Size Code )
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // overriden method of GLSurfaceView.Renderer ( Rendering Code )
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }
    
    // Handling 'onTouchEvent' Is The Most IMPORTANT,
    // Because It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // code
        int eventaction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        //System.out.println("VDG: "+"Single Tap");
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do Not Write Any code Here Because Already Written 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
        //System.out.println("VDG: "+"Long Press");
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        //System.out.println("VDG: "+"Scroll");
		uninitialize();
        System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
    
    private void initialize(GL10 gl)
    {
		//******************Vertex Shader************'
		//create shader
		vertexShaderObject = GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);
		
		// vertex shader source code
		final String vertexShaderSourceCode= String.format
		(
		 "#version 310 es"+
		 "\n"+
		 "void main(void)"+
		 "{"+
		 "}"
		 );
		 
		 //provide source code to shader
		 GLES31.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		 
		 // compile shader and check errors
		 GLES31.glCompileShader(vertexShaderObject);
		 int[] iShaderCompileStatus = new int[1];
		 int[] iInfoLogLength = new int[1];
		 String szInfoLog = null;
		 
		 GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		 
		 if(iShaderCompileStatus[0] == GLES31.GL_FALSE)
		 {
			 GLES31.glGetShaderiv(vertexShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			 if(iInfoLogLength[0]>0)
			 {
				 szInfoLog = GLES31.glGetShaderInfoLog(vertexShaderObject);
				 System.out.println("PBB: Vertex Shader Compilation log = "+szInfoLog);
				 uninitialize();
				 System.exit(0);
			 }
		 }
		 
		 // ***********************************************
        // Fragment Shader
        // ***********************************************
        // create shader
        fragmentShaderObject=GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);
        
        // fragment shader source code
        final String fragmentShaderSourceCode =String.format
        (
         "#version 310 es"+
         "\n"+
         "void main(void)"+
         "{"+
         "}"
        );
        
        // provide source code to shader
        GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
        
        // compile shader and check for errors
        GLES31.glCompileShader(fragmentShaderObject);
        iShaderCompileStatus[0] = 0; // re-initialize
        iInfoLogLength[0] = 0; // re-initialize
        szInfoLog=null; // re-initialize
        GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES31.GL_FALSE)
        {
            GLES31.glGetShaderiv(fragmentShaderObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES31.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
		 
		 //create shader program
		 shaderProgramObject = GLES31.glCreateProgram();
		 
		 //attach shaders
		 GLES31.glAttachShader(shaderProgramObject, vertexShaderObject);
		 GLES31.glAttachShader(shaderProgramObject, fragmentShaderObject);
		 		 
		 //link the two shaders together to shader program
		 GLES31.glLinkProgram(shaderProgramObject);
		 int[] iShaderProgramLinkStatus =new int[1];
		 iInfoLogLength[0] = 0; // re-initialize
		 szInfoLog = null;
		 
		 GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		 if(iShaderProgramLinkStatus[0] == GLES31.GL_FALSE)
		 {
			 GLES31.glGetProgramiv(shaderProgramObject, GLES31.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			 if(iInfoLogLength[0] > 0)
			 {
				 szInfoLog = GLES31.glGetShaderInfoLog(shaderProgramObject);
				 System.out.println("VDG: Shader Program Link Log= "+szInfoLog);
				 uninitialize();
				 System.exit(0);
			 }
		 }
		 
		// enable depth testing
        GLES31.glEnable(GLES31.GL_DEPTH_TEST);
        // depth test to do
        GLES31.glDepthFunc(GLES31.GL_LEQUAL);
        // We will always cull back faces for better performance
        GLES31.glEnable(GLES31.GL_CULL_FACE);
	
        // Set the background frame color
        GLES31.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		
		// set projectionMatrix to identitu matrix
        Matrix.setIdentityM(orthographicProjectionMatrix,0);
    }
    
    private void resize(int width, int height)
    {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES31.glViewport(0, 0, width, height);
		
		// Orthographic Projection => left,right,bottom,top,near,far
        if (width <= height)
            Matrix.orthoM(orthographicProjectionMatrix,0,-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)), -100.0f, 100.0f);
        else
            Matrix.orthoM(orthographicProjectionMatrix,0,(-100.0f * (width / height)), (100.0f * (width / height)), -100.0f, 100.0f, -100.0f, 100.0f);
    }
	
    public void display()
    {
        // Draw background color
        GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT | GLES31.GL_DEPTH_BUFFER_BIT);
		
		// use shader program
        GLES31.glUseProgram(shaderProgramObject);

  
        // un-use shader program
        GLES31.glUseProgram(0);

        // render/flush
        requestRender();
    }
	
	
	void uninitialize()
    {
        // code
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // detach vertex shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, vertexShaderObject);
                // delete vertex shader object
                GLES31.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }
            
            if(fragmentShaderObject != 0)
            {
                // detach fragment  shader from shader program object
                GLES31.glDetachShader(shaderProgramObject, fragmentShaderObject);
                // delete fragment shader object
                GLES31.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // delete shader program object
        if(shaderProgramObject != 0)
        {
            GLES31.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

}
